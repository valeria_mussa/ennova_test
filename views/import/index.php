<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Alert;
use yii\bootstrap4\Html;

$this->title = Yii::t('import','page_title');

?>
<div class="import-index">
    <h1><?= Html::encode($this->title) ?></h1>
    
    <?= Alert::widget([
			'id' => 'message',
			'options' => [ 'class' => 'd-none' ],
			'body' => '<div class="alert-body overflow-auto" style="max-height: 100px"></div>',
		]); 
	?>
	
    <div style="color:#999;">
        <?= Yii::t('import', 'import_description') ?>
    </div>

    <?php $form = ActiveForm::begin([
		'id' => 'import-form',
        'action' => ['import/import'],
        'method' => 'post',
        'layout' => 'horizontal',
        'options' => [ 'class' => 'bg-light p-2 col-8', 'enctype' => 'multipart/form-data' ]
    ]); ?>
        
         <?= $form->field($model, 'file')->fileInput()->label(Yii::t('import','file')) ?>
			
        <?= $form->field($model, 'table')->dropDownList($model->getTables())->label(Yii::t('import','table')) ?>       
        
        <?= $form->field($model, 'truncate')->checkbox()->label(Yii::t('import','truncate')) ?>       

        <div class="form-group">
            <?= Html::submitButton(
				'<i class="fa-solid fa-arrow-up-from-bracket mr-2"></i>'.
				Yii::t('import','import_btn'), 
				['class' => 'btn btn-primary' ]
			) ?>
        </div>

    <?php ActiveForm::end(); ?>
    
</div>

<?php

$js = <<<'ENDJS'
$(document).ready(function() {
	$('#import-form').on('beforeSubmit',function() {
	//return true;
		const form = this;
		const $form = $(this);
		const $alert = $("#message");
		if ($form.find('.has-error').length) {
			return false;
		}
		let data = new FormData(form);		
		$alert.removeClass("alert-success alert-danger").addClass("d-none");
		$.ajax({
			url: $form.attr('action'),
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			method: 'POST',
			type: 'POST', // For jQuery < 1.9
			success: function(data){
				$alert.removeClass("d-none").addClass( (data.success) ? "alert-success" : "alert-danger" );
				$alert.find(".alert-body").text(data.message);
				form.reset();
			}
		}).fail(function(data) {
			$alert.removeClass("d-none").addClass("alert-danger");
			$alert.find(".alert-body").text(data.statusText);
		});
		return false;
	});
});
ENDJS;

$this->registerJs($js); ?>
