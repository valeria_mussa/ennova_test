<?php
use kartik\export\ExportMenu;
use yii\bootstrap4\Html;

/***
	@var 	yii\web\View 					$this
	@var 	yii\data\ActiveDataProvider 	$dataProvider
	@var 	Array						 	$columns
***/

$headerStyle = [
    'font' => ['bold' => true],
    'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
        'color' => [
            'argb' => \PhpOffice\PhpSpreadsheet\Style\Color::COLOR_WHITE,
        ],
    ],
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => ['argb' => 'FFdee2e6' ],
        ],
        'inside' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_MEDIUM,
            'color' => ['argb' => 'FFdee2e6' ],
        ]
    ]
];

$boxStyle = $headerStyle;
$boxStyle['font']['bold'] = false;

?>
<?=
ExportMenu::widget([
	'dataProvider' => $dataProvider,
	'columns' => $columns,
	'pjaxContainerId' => 'kv-pjax-container',
	'dropdownOptions' => [
		'icon' => '<i class="fa-solid fa-download"></i>',
		'label' => '',
		'class' => 'btn-primary text-white',
		'itemsBefore' => [
			'<div class="dropdown-header">'. Yii::t('pratica','export_full') .'</div>',
		],
	],
	'exportConfig' => [
		ExportMenu::FORMAT_EXCEL => false,
		ExportMenu::FORMAT_HTML => false,
		ExportMenu::FORMAT_TEXT => false,	
		ExportMenu::FORMAT_EXCEL_X => [
			'label' => 'Excel'
		],	
    ],
    'headerStyleOptions' => [
		ExportMenu::FORMAT_EXCEL_X => $headerStyle,
		ExportMenu::FORMAT_PDF => $headerStyle
    ],
    'boxStyleOptions' => [
		ExportMenu::FORMAT_EXCEL_X => $boxStyle,
		ExportMenu::FORMAT_PDF => $boxStyle
    ],
	'showConfirmAlert' => false,
	'onInitSheet' => function($sheet, $widget) {
		$sheet->getDefaultRowDimension()->setRowHeight(30);
		$sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
		$sheet->getPageSetup()->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
	}
]);
?>


