<?php 

use kartik\grid\GridView;
use yii\bootstrap4\Html;
use yii\helpers\Url;
use kartik\grid\GridViewTrait;

/***
	@var 	yii\web\View 					$this
	@var 	app\models\PraticaSearch 		$model
	@var 	yii\data\ActiveDataProvider 	$dataProvider
	@var 	yii\data\ActiveDataProvider 	$dataProviderAll
	@var 	Array						 	$columns
***/


$this->title = Yii::t('pratica','page_title');

$template = "
<div class='pratica-index' id='tutto'>
	<div class='row'>
		<div class='col-6'>
			<h1>".  Html::encode($this->title) ."</h1>
		</div>
		<div class='col-6 text-right' id='export-container'>
			" . $this->render('_export', ['dataProvider' => $dataProviderAll, 'columns' => $columns ]) . "
			{export}
			</div>
		</div>
	 </div>
	 <div class='row'>
		<div class='col-sm-12 col-md-4 col-lg-3'>
			" . $this->render('_search', ['praticaSearch' => $praticaSearch]) ."
		</div>
		<div class='col-sm-12 col-md-8 col-lg-9' id='grid'>	
			 <div class='text-right mb-2'>{summary}</div>
			 {items}
			 <div class='d-flex justify-content-end align-items-center mb-4'>
				<div class='pager-label mr-2'>".Yii::t('pratica','go_page')."</div>
				<div>{pager}</div>
			</div>
		</div>
	</div>
</div>
";
?>

<?=	
GridView::widget([	
	'dataProvider' => $dataProvider,
	'columns' => $columns,
	'pjax' => true,
	'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container']],
	'layout' => $template,
	'export' => [ 
		'label' => Yii::t('pratica','export_view') ,
		'showConfirmAlert' => false,
		'target' => GridView::TARGET_SELF
	],
	'exportConfig' => [
		GridView::CSV => [
			'config' => [
				'colDelimiter' => ','
			],
		],
		GridView::EXCEL => [],
		GridView::PDF => [
			'config' => [
				'methods' => [
					'SetTitle' => $this->title,
					'SetHeader'=> '',
					'SetFooter'=> '',
				],
				'options' => [
					'ignore_table_widths' => true,
					'ignore_table_percents' => true
				]
			]
		],
		GridView::JSON => [
			'config' => [
				'slugColHeads' => true
			]
		]
    ],
	'pager' => [ 'class' => '\yii\bootstrap4\LinkPager' ]
]);
?>


<!-- MODALE PER DETTAGLI --->
<?= $this->render('_viewContainer', []); ?>
<!-- //END DETTAGLI -->



<?php
$js = <<<'ENDJS'

$(document).ready(function(){

	/******** RILEVO CAMBIAMENTI NEL PAGER PER MODIFICHE GRAFICHE ALLA LISTA **********/
	 $(document).on("pjax:end", function() {
		editViewStyle(); 
	});
	editViewStyle();
});

function editViewStyle() {
	/* pager: mostro / nascondo label nel caso di una sola pagina presente */
	uploadPagerLabel();
	/* bottoni esportazione: rimuovo classe btn-outline-secondary inserita di default */
	changeExportBtnStyle();
	
	/******** ATTACCO EVENTO AL BOTTONE 'DETTAGLI' DEI RECORD PER VISUALIZZARE I DETTAGLI *********/
	$(".show-details").off("click").click(function() {
		const url = $(this).attr("data-url");
		$.get(url,function(data) {
			let title = $(data).filter("title").text();
			let html = $(data).filter("main").html();
			showModal(title,html);
		}).fail(function(data) {			
			let title = $(data).filter("title").text();
			let html = data.responseText;			
			showModal(title,html);
		});
		return false;
	});
}


function uploadPagerLabel() {
	const $grid = $("#grid");
	const $label = $grid.find(".pager-label");
	const $pager = $grid.find(".pagination").length;
	$label.removeClass("d-block d-none").addClass( $pager>0 ? "d-block" : "d-none");
}

function changeExportBtnStyle() {
	$("#export-container .btn-outline-secondary").removeClass("btn-outline-secondary").addClass("btn-outline-primary");
}

function showModal(title,html) {
	$("#details .modal-title").html(title);
	$("#details .modal-body").html(html);
	$(".modal .tooltip-btn").tooltip({
		html: true
	});
	$("#details").modal("show");
}
ENDJS;

$this->registerJs($js);
?>




