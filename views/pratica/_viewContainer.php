<?php 

use yii\bootstrap4\Modal;
use yii\bootstrap4\Button;

?>

<?php
Modal::begin([
	'id' => 'details',
	'scrollable' => true,
	'centerVertical' => true,
	'options' => [
		'data-backdrop' => 'static'
	],
	'title' => true,
	'headerOptions' => [ 'class' => 'bg-dark text-white' ],
	'closeButton' => [ 'class' => 'close text-white' ],
	'footer' => Button::widget([
		'label' => "<i class='fa-solid fa-times mr-2'></i>".Yii::t('pratica','close_btn'),
		'encodeLabel' => false,
		'options' => ['class' => 'btn-sm btn-primary', 'data-dismiss' => 'modal' ],
	])
]); 
?>
<?php Modal::end(); ?>
