<?php 

use kartik\helpers\Html;
use yii\widgets\DetailView;

$this->title = Yii::t('pratica','detail_title_{id_pratica}', [ 'id_pratica' => $pratica['id'] ]);

/***
	@var 	yii\web\View 	$this
	@var 	Array			$pratica
***/

?>
<?= DetailView::widget([
	'model' => $pratica,
	'attributes' => [
		[
			'attribute' => 'id_pratica',
			'label' => Yii::t('pratica','id_pratica'),
			'format' => function($data) {
				return '#'.$data;
			},
		],
		[
			'attribute' => 'data_creazione',
			'label' => Yii::t('pratica','data_creazione'),
			'format' => 'datetime'
		],
		[
			'attribute' => 'stato_pratica',
			'label' => Yii::t('pratica','stato_pratica'),
			'format' => function($data) {
				return Yii::t('pratica',$data);
			},
		],
		[
			'label' => Yii::t('pratica','cliente'),
			'value' => function($model) {
				if (!$model['cliente'] || trim($model['cliente']['note'])=="") $note = "";
				else $note = "<i class='fa-solid fa-circle-info ml-2 text-primary cursor-pointer tooltip-btn' title=\"".$model['cliente']['note']."\"></i>";
				return ($model['cliente']) ? $model['cliente']['nome']." ".$model['cliente']['cognome']." ".$note : null; 
			},
			'format' => 'html'
		],
		[
			'label' => Yii::t('pratica','codice_fiscale'),
			'value' =>  function($model) {
				return ($model['cliente']) ? $model['cliente']['codice_fiscale'] : null;
			}
		],
		[
			'attribute' => 'pratica.note',
			'label' => Yii::t('pratica','note'),
			'format' => function($data) {
				return (trim($data)=="") ? "-" : $data;
			}
		]
	]
]); ?>
