<?php

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use yii\helpers\Url;

/***
	@var 	yii\web\View 				$this
	@var 	app\models\PraticaSearch 	$model
***/

?>
<?php $form = ActiveForm::begin([
	'id' => 'search-form',
	'method' => 'get',
	'action' => Url::to(['pratica/index']),
	'options' => [ 'class' => 'bg-light p-2', 'data-pjax' => true ]
])
?>
<?= $form->field($praticaSearch, 'id_pratica')->textInput()->label(Yii::t('pratica','id_pratica')) ?>
<?= $form->field($praticaSearch, 'codice_fiscale')->textInput()->label(Yii::t('pratica','codice_fiscale')) ?>

<div class="form-group">
	<?= Html::submitButton("<i class='fa-solid fa-search mr-2'></i>".Yii::t('pratica','search'), ['class' => 'btn btn-primary' ]) ?>
</div>

<?php ActiveForm::end(); ?>
