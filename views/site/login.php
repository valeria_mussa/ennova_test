<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

$this->title = Yii::t('site','page_title_login');

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <div style="color:#999;">
        <?= Yii::t('site', 'login_description{admin}{user}', [ 'admin' => 'admin/admin', 'user' => 'demo/demo' ]) ?>
    </div>

    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'options' => [ 'class' => 'bg-light p-2 col-8' ]
    ]); ?>

        <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(Yii::t('site','username')) ?>

        <?= $form->field($model, 'password')->passwordInput()->label(Yii::t('site','password')) ?>

        <?= $form->field($model, 'rememberMe')->checkbox()->label(Yii::t('site','remember_me')) ?>

        <div class="form-group">
            <?= Html::submitButton(
				'<i class="fa-solid fa-right-to-bracket mr-2"></i>'.
				Yii::t('site','login_btn'), 
				['class' => 'btn btn-primary', 'name' => 'login-button' ]
			) ?>
        </div>

    <?php ActiveForm::end(); ?>

    
</div>
