<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pratica}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%cliente}}`
 */
class m220222_000002_create_pratica_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pratica}}', [
            'id' => $this->primaryKey(),
            'id_pratica' => $this->string(16)->notNull()->unique(),
            'data_creazione' => $this->datetime()->notNull()->defaultExpression('NOW()'),
            'stato_pratica' => "ENUM('open','close') NOT NULL DEFAULT 'open'",
            'note' => $this->text(),
            'id_cliente' => $this->integer(),
        ]);

        // creates index for column `stato_pratica`
        $this->createIndex(
            '{{%idx-pratica-id_cliente}}',
            '{{%pratica}}',
            'id_cliente'
        );

        // add foreign key for table `{{%cliente}}`
        $this->addForeignKey(
            '{{%fk-pratica-id_cliente}}',
            '{{%pratica}}',
            'id_cliente',
            '{{%cliente}}',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%cliente}}`
        $this->dropForeignKey(
            '{{%fk-pratica-id_cliente}}',
            '{{%pratica}}'
        );

        // drops index for column `stato_pratica`
        $this->dropIndex(
            '{{%idx-pratica-id_cliente}}',
            '{{%pratica}}'
        );

        $this->dropTable('{{%pratica}}');
    }
}
