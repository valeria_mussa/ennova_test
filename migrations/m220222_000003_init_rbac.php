<?php

use yii\db\Migration;

/**
 * Class m220222_000003_init_rbac
 */
class m220222_000003_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
		$auth = Yii::$app->authManager;
		
		$auth->removeAll();
				
		$showList = $auth->createPermission('showList');
		$showList->description = 'Show list page and export data';
		$auth->add($showList);
		
		$import = $auth->createPermission('import');
		$import->description = 'Import database data';
		$auth->add($import);
		
		$generic_user = $auth->createRole('generic_user');
		$auth->add($generic_user);
		$auth->addChild($generic_user,$showList);
		
		$admin = $auth->createRole('admin');
		$auth->add($admin);
		$auth->addChild($admin,$import);
		$auth->addChild($admin,$generic_user);
		
		$auth->assign($generic_user,'101');
		$auth->assign($admin,'100');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220222_214420_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
