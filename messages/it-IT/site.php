<?php 

return [

	//LOGIN
	'page_title_login' => 'Login',
	'login_description{admin}{user}' => 'Puoi effettuare il login con <code>admin/admin</code> o <code>demo/demo</code>.',
	'username' => 'Nome utente',
	'password' => 'Password',
	'remember_me' => 'Non chiedere di nuovo',
	'login_btn' => 'Login',
	'error_wrong_user_or_pwd' => 'Username o password errata.',
	
	//LOGOUT
	'page_title_logout{username}' => 'Logout ({username})',
	
	//ERROR
	'error_processing' => "L'errore si è verificato mentre il Web Server stava processando la tua richiesta.",
	'error_contact_us' => "Per favore, contattaci se pensi che sia un problema del server. Grazie mille.",
];
