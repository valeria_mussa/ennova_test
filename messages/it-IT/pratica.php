<?php

return [

	//INDEX
	'page_title' => 'Lista Pratiche',		
	'go_page' => 'Vai alla pagina:',	
	'export_full' => 'Esporta tutti i dati',
	'export_view' => 'Esporta vista',
	'search' => 'Cerca',
	
	//DB FIELDS
	'id' => 'ID',
	'id_pratica' => 'ID Pratica',
	'codice_fiscale' => 'Codice Fiscale / P. IVA',
	'data_creazione' => 'Data di creazione',
	'cliente' => 'Cliente',
	'stato_pratica' => 'Stato',
	'open' => 'Aperta',
	'close' => 'Chiusa',
	'note' => 'Note di lavorazione',
	
	//VIEW	
	'detail_title_{id_pratica}' => 'Pratica #{id_pratica}',	
	'close_btn' => 'Chiudi',
	
	//ERRORS	
	'error_not_found' => 'La pratica cercata non esiste.',
];
