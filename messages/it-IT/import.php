<?php
return [

	//IMPORT
	'page_title' => 'Importazione dati',
	'import_description' => 'Scegli il file con i dati da importare.',
	'table' => 'Tabella',
	'file' => 'File',
	'truncate' => 'Cancella tutti i dati presenti nella tabella prima di procedere',
	'import_btn' => 'Importa',
	
	//ERROR	
	'error_invalid_request' => 'Richiesta non valida',
	'error_cannot_load_model' => 'Impossibile caricare il modello',
	'error_cannot_upload_file' => 'Impossibile caricare il file',
	'error_cannot_import' => 'Impossibile importare i dati',
	'error_invalid_file' => 'File non valido',	
	
	//SUCCESS
	'success_message' => 'Dati importati correttamente!',
	
];
?>
