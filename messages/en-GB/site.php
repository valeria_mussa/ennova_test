<?php 

return [

	//LOGIN
	'page_title_login' => 'Login',
	'login_description{admin}{user}' => 'You may login with <code>{admin}</code> or <code>{user}</code>.',
	'username' => 'Username',
	'password' => 'Password',
	'remember_me' => 'Remeber me',
	'login_btn' => 'Login',
	'error_wrong_user_or_pwd' => 'Incorrect username or password.',
	
	//LOGOUT
	'page_title_logout{username}' => 'Logout ({username})',
	
	//ERROR
	'error_processing' => 'The above error occurred while the Web server was processing your request.',
	'error_contact_us' => 'Please contact us if you think this is a server error. Thank you.'
];
