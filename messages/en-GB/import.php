<?php
return [

	//IMPORT
	'page_title' => 'Data import',
	'import_description' => 'Choose the file with the data.',
	'table' => 'Table',
	'file' => 'File',
	'truncate' => 'Truncate table before',
	'import_btn' => 'Import',
	
	//ERROR	
	'error_invalid_request' => 'Invalid request',
	'error_cannot_load_model' => 'Invalid model',
	'error_cannot_upload_file' => 'Cannot upload the file',
	'error_cannot_import' => 'Cannot import data',
	'error_invalid_file' => 'Invalid file',	
	
	//SUCCESS
	'success_message' => 'Data imported successfully!',
	
];
?>
