<?php

return [

	//INDEX
	'page_title' => 'Ticket List',		
	'go_page' => 'Go to page:',	
	'export_full' => 'Export all',
	'export_view' => 'Export view',
	'search' => 'Search',
	
	//DB FIELDS
	'id' => 'ID',
	'id_pratica' => 'Ticket ID',
	'codice_fiscale' => 'Fiscal code / VAT',
	'data_creazione' => 'Creation date',
	'cliente' => 'Customer',
	'stato_pratica' => 'Status',
	'open' => 'Open',
	'close' => 'Close',
	'note' => 'Worknotes',
	
	//VIEW	
	'detail_title_{id_pratica}' => 'Ticket #{id_pratica}',	
	'close_btn' => 'Close',
	
	//ERRORS	
	'error_not_found' => "The ticket requested doesn't exists.",
];
