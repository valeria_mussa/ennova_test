--
-- Dump dei dati per la tabella `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cognome`, `codice_fiscale`, `note`) VALUES
(1, 'Giuliana', 'Ferri', 'FRRGLN74B68D643G', NULL),
(2, 'Silvio', 'Genovesi', 'GNVSLV88P25A859P', NULL),
(3, 'Patrizia', 'Nucci', 'NCCPRZ83A68G478I', NULL),
(4, 'Raffaele', 'Costa', 'CSTRFL61C05H294M', NULL),
(5, 'Martina', 'Piazza', 'PZZMTN62H58L219E', NULL),
(6, 'Fausto', 'Colombo', 'CLMFST79E20A001F', NULL),
(7, 'Veronica', 'Lorenzo', 'LRNVNC62C67D612X', NULL),
(8, 'Renata', 'Fanucci', 'FNCRNT85C52G224S', NULL),
(9, 'Luciano', 'Mazzanti', 'LCNMZN72D27D643S', NULL);

