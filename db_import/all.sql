--
-- Dump dei dati per la tabella `cliente`
--

INSERT INTO `cliente` (`id`, `nome`, `cognome`, `codice_fiscale`, `note`) VALUES
(1, 'Giuliana', 'Ferri', 'FRRGLN74B68D643G', NULL),
(2, 'Silvio', 'Genovesi', 'GNVSLV88P25A859P', NULL),
(3, 'Patrizia', 'Nucci', 'NCCPRZ83A68G478I', NULL),
(4, 'Raffaele', 'Costa', 'CSTRFL61C05H294M', NULL),
(5, 'Martina', 'Piazza', 'PZZMTN62H58L219E', NULL),
(6, 'Fausto', 'Colombo', 'CLMFST79E20A001F', NULL),
(7, 'Veronica', 'Lorenzo', 'LRNVNC62C67D612X', NULL),
(8, 'Renata', 'Fanucci', 'FNCRNT85C52G224S', NULL),
(9, 'Luciano', 'Mazzanti', 'LCNMZN72D27D643S', NULL);

-- --------------------------------------------------------

--
-- Dump dei dati per la tabella `pratica`
--

INSERT INTO `pratica` (`id`, `id_pratica`, `data_creazione`, `stato_pratica`, `note`, `id_cliente`) VALUES
(148955948, '4444045', '2022-01-30 11:17:04', 'open', NULL, 1),
(148955949, '4444046', '2022-02-01 11:17:04', 'open', NULL, 3),
(148955950, '4444047', '2022-02-02 11:16:21', 'open', NULL, 2),
(148955951, '4444048', '2022-02-09 11:16:21', 'open', NULL, 8),
(148955952, '4444049', '2022-02-09 11:20:31', 'close', NULL, 4),
(148955953, '4444050', '2022-02-10 11:20:19', 'close', NULL, 9),
(148955954, '4444051', '2022-02-12 11:17:04', 'open', NULL, 1),
(148955955, '4444052', '2022-02-15 11:20:19', 'close', NULL, 9),
(148955956, '4444053', '2022-02-16 11:17:04', 'open', NULL, 1),
(148955957, '4444054', '2022-02-17 11:17:04', 'open', NULL, 3),
(148955958, '4444055', '2022-02-17 11:16:21', 'open', NULL, 2),
(148955959, '4444056', '2022-02-17 11:16:21', 'open', NULL, 8),
(148955960, '4444057', '2022-01-19 11:20:31', 'close', NULL, 4),
(148955961, '4444058', '2022-02-20 11:20:19', 'close', NULL, 9),
(148955962, '4444059', '2022-02-21 11:15:43', 'open', NULL, 6),
(148955963, '4444060', '2022-02-21 11:15:43', 'open', NULL, 7),
(148955964, '4444061', '2022-02-21 11:16:21', 'open', NULL, 8),
(148955965, '4444062', '2022-02-21 11:16:21', 'open', NULL, 2),
(148955966, '4444063', '2022-02-21 11:17:04', 'open', NULL, 3),
(148955967, '4444064', '2022-02-21 11:17:04', 'open', NULL, 1);
