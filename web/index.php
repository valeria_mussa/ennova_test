<?php
//come prima cosa, verifico se il db esiste
try {	
	$db_config = include '../config/db.php';
	$pdo = new PDO($db_config['dsn'], $db_config['username'], $db_config['password']);
	$pdo = null;
} catch (Exception $e) {
	header("Location: init.php");
	die();
}

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';


(new yii\web\Application($config))->run();
