<?php
$error = [];

$filename = '../config/db.php';

$host = 'localhost';
$db = 'mv_ennova';
$username = 'root';
$password = '';

//AL CARICAMENTO DELLA PAGINA
//verifico che il db non esista
//se il database esiste, torno su index.php

if (empty($_POST)) {
	$dns = 'mysql:host='.$host.';dbname='.$db;	
	try {
		$pdo = new PDO($dns, $username, $password );
		$pdo = null;				
		$success = true;	
	} catch (PDOException $e) { 
		//non fare nulla
	}
}

//ALL'INVIO DEL FORM
else {	
	$host = $_POST['host'];
	$db = $_POST['db'];
	$username = $_POST['username'];
	$password = $_POST['password'];
	
	if (empty($host)) $error['host'] = 'Il campo non può essere vuoto.';
	if (empty($db)) $error['db'] = 'Il campo non può essere vuoto.';
	if (empty($username)) $error['username'] = 'Il campo non può essere vuoto.';
	
	//verifico privilegi in varie cartelle
	if (empty($error)) {		
		try {
			$filetest = '../runtime/test.txt';
			$file = fopen($filetest,'a+');
			fclose($file);
			unlink($filetest);
		} catch (Throwable $e) {
			$error['message'] = 
				'Aggiungi i permessi in lettura / scrittura sulla cartella <code>./runtime</code> prima di procedere.';
		}		
	}
	if (empty($error)) {		
		try {
			$filetest = './assets/test.txt';
			$file = fopen($filetest,'a+');
			fclose($file);
			unlink($filetest);
		} catch (Throwable $e) {
			$error['message'] = 
				'Aggiungi i permessi in lettura / scrittura sulla cartella <code>./web/assets</code> prima di procedere.';
		}		
	}
	
	//verifico se è possibile scrivere il file di configurazione
	if (empty($error)) {		
		try {
			$file = fopen($filename,'a+');
			fclose($file);
		} catch (Throwable $e) {
			$error['message'] = 
				'Aggiungi i permessi in lettura / scrittura sulla cartella <code>./config</code> prima di procedere.';
		}		
	}
			
	
	//provo a creare il database
	if (empty($error)) {		
		try {
			$pdo = new PDO("mysql:host=$host", $username, $password);

			$pdo->exec("CREATE DATABASE `$db`;
					GRANT ALL ON `$db`.* TO '$username'@'localhost';
					FLUSH PRIVILEGES;")
			or $error['message'] = $pdo->errorInfo();
		} catch (PDOException $e) {
			$error['message'] =  $e->getMessage();
		}
	}
	
	//provo a scrivere il file di configurazione
	if (empty($error)) {		
		
		$php = "<?php
			return [
				'class' => 'yii\db\Connection',
				'dsn' => 'mysql:host={$host};dbname={$db}',
				'username' => '{$username}',
				'password' => '{$password}',
				'charset' => 'utf8',
				'attributes'=>array(
					PDO::MYSQL_ATTR_LOCAL_INFILE => true
				)
			];
		";
		
		try {
			$file = fopen($filename,'w+');
			fwrite($file,$php);
			fclose($file);
		} catch (Throwable $e) {
			@unlink($filename);
			$error['message'] = $e->getMessage();
		}
		
	}
	
	//verifico se la procedura è avvenuta con successo o no
	$success = empty($error);	
		
}

$alert_class = (isSet($success)) ? (($success) ? 'alert-success' : 'alert-danger') : 'd-none';
$form_class = (isSet($success) && $success) ? 'd-none' : '';
?>
<!DOCTYPE html>
<html lang="it-IT" class="h-100">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Creazione db</title>
	<link href="assets/d896448e/css/bootstrap.css" rel="stylesheet">
	<link href="css/site.css" rel="stylesheet">
	<link href="css/fontawesome/css/all.min.css" rel="stylesheet">
</head>
<body class="d-flex flex-column h-100">
<header>
    <nav id="w0" class="navbar navbar-expand-md navbar-dark bg-dark sticky-top navbar">
		<div class="container">
			<a class="navbar-brand" href="index.php"><img src="images/logo.png" alt=""></a>
		</div>
	</nav>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
		<div class="init">
			<h1>Creazione db</h1>
			
			<div class="alert <?= $alert_class ?>" role="alert">
				<?= ($success) ? 
					'Database creato con successo!<br>
					Lancia <code>./yii migrate</code> per importare le tabelle.<br>
					<a href="index.php" class="btn btn-success mt-2">Vai alla home</a>' 
						: 
				$error['message'] 
				?>
			</div>

			<div style="color:#999" class="<?= $form_class ?>">
				Prima di iniziare, devi creare il db.
			</div>

			<form class="bg-light p-2 col-8 <?= $form_class ?>" action="init.php" method="post">
				<div class="form-group row required align-items-center">
					<label class="col-sm-2 mb-0" for="form-host">Host</label>
					<div class="col-sm-10">
						<input type="text" id="form-host" class="form-control <?= isSet($error['host']) ? 'is-invalid' : '' ?>" name="host" autofocus aria-required="true" value="<?= $host ?>">
						<div class="invalid-feedback "><?= $error['host'] ?? '' ?></div>
					</div>
				</div>
				<div class="form-group row required align-items-center">
					<label class="col-sm-2 mb-0" for="form-db">Database</label>
					<div class="col-sm-10">
						<input type="text" id="form-db" class="form-control <?= isSet($error['db']) ? 'is-invalid' : '' ?>" name="db" autofocus aria-required="true" value="<?= $db ?>">
						<div class="invalid-feedback "><?= $error['db'] ?? '' ?></div>
					</div>
				</div>
				<div class="form-group row required align-items-center">
					<label class="col-sm-2 mb-0" for="form-username">Username</label>
					<div class="col-sm-10">
						<input type="text" id="form-username" class="form-control <?= isSet($error['username']) ? 'is-invalid' : '' ?>" name="username" autofocus aria-required="true" value="<?= $username ?>">
						<div class="invalid-feedback "><?= $error['username'] ?? '' ?></div>
					</div>
				</div>
				<div class="form-group row required align-items-center">
					<label class="col-sm-2 mb-0" for="form-password">Password</label>
					<div class="col-sm-10">
						<input type="password" id="form-password" class="form-control <?= isSet($error['password']) ? 'is-invalid' : '' ?>" name="password" autofocus aria-required="true">
						<div class="invalid-feedback "><?= $error['password'] ?? '' ?></div>
					</div>
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary">
						<i class="fa-solid fa-right-to-bracket mr-2"></i>
						Crea
					</button>
				</div>
			</form>
				
		</div>
    </div>
</main>

<script src="assets/6699cb4b/jquery.js"></script>
<script src="assets/d896448e/js/bootstrap.bundle.js"></script>
</body>
</html>
