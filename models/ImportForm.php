<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\validators\FileValidator;

class ImportForm extends Model {
	public $table;
	public $file;
	public $truncate;
	
	private $_filename;
	
	public function rules() {
		$mb = 1;
		$maxFileSize = 1024 * 1024 * $mb;
		return [
			[ ['file', 'table', 'truncate' ], 'safe'],
			[ ['file', 'table' ], 'required'],
			[ 
				'file', 'file',
				'checkExtensionByMimeType' => true,
				'mimeTypes' => array_keys($this->getFileTypes()),
				'extensions' => array_values($this->getFileTypes()),
				'maxSize' => $maxFileSize,
				'skipOnEmpty' => false
			]
		];
	}
	
	public function upload() {
        if ($this->validateUpload()) {
			$this->_filename = Yii::$app->basePath . '/runtime/' . uniqid('file_'). '.' . $this->file->extension;
            $this->file->saveAs($this->_filename);           
            return true;
        } 
        return false;
    }
    
    private function validateUpload() {
		$mime = $this->file->type;
		$mimeTypes = array_keys($this->getFileTypes());
		if (!in_array($mime,$mimeTypes)) return false;		
		if ($this->table == null) return false;		
		return true;
	}
	
	private function getFileTypes() {
		return [
			"application/sql" => "SQL",
			"text/csv" => "CSV"
		];
	}	
	
	public function getTables() {
		//tabelle da escludere per l'import
		$exclude_tables = [ 'migration' ];
		//recupero tabella
		$schema = Yii::$app->db->getSchema();
		$tables = $schema->getTableNames();
		//escludo tabelle non necessarie
		$needed_tables = array_diff($tables,$exclude_tables);
		//ordino risultati
		sort($needed_tables, SORT_STRING);
		return $needed_tables;
	}		
	
	public function getTable() {
		$tables = $this->getTables();
		return $tables[intval($this->table)];
	}
	public function truncate() {
		if (intval($this->truncate)>0) {
			try {
				Yii::$app->db->createCommand('set foreign_key_checks=0')->execute();
				Yii::$app->db->createCommand()->truncateTable($this->getTable())->execute();
				Yii::$app->db->createCommand('set foreign_key_checks=1')->execute();
				return true;
			} catch (Exception $e) { 
				//no action on failure
			}
		}
		
		return false;		
	}
	
	public function insert(&$sql) {
		switch ($this->file->type) {
			case 'text/csv':
				return $this->insertCSV($sql);
			case 'application/sql':
				return $this->insertSQL($sql);
			default:
				return false;
		}
	}
	
	private function insertCSV(&$sql) {
		//leggo prima riga del file per recuperare le colonne
		$f = fopen($this->_filename, 'r');
		$header = fgets($f);
		fclose($f);
		//recupero separatore csv
		$separator = (substr_count($header,',')>0) ? ',' : ';';
		//recupero colonne
		$columns = explode($separator,str_replace('"','`',$header));
		
		//costruisco query mysql
		//NB vale solo per mysql
		$sql= "LOAD DATA LOCAL INFILE '".$this->_filename."'
			   INTO TABLE `".$this->getTable()."`
			   FIELDS
					TERMINATED BY '".$separator."'
					ENCLOSED BY '\"'
			   LINES
					TERMINATED BY '\n'
			   IGNORE 1 LINES
			   (".implode(',',$columns).")";
         
		$con = Yii::$app->db;
		
		//provo ad eseguire la query (in caso di problemi: rollback)
		$transaction = $con->beginTransaction();
		try {
			$con->createCommand($sql)->execute();
			$transaction->commit();
		} catch(Exception $e) {
			$transaction->rollBack();
			throw new Exception(Yii::t('import','error_invalid_file'));
			return false;
		}
		return true;
	}
	
	private function insertSQL(&$sql) {
		//leggo il file
		$sql = file_get_contents($this->_filename);
		
		//eseguo SQL per ogni riga
		$con = Yii::$app->db;		
		$transaction = $con->beginTransaction();
		try {
			$con->createCommand($sql)->execute();
			$transaction->commit();
		} catch(Exception $e) {
			$transaction->rollBack();
			throw new Exception(Yii::t('import','error_invalid_file'));
			return false;
		}
		return true;
	}
	
	public function removeFile() {
		try {
			unlink($this->_filename);
			$this->_filename = null;
			$this->file = null;
			return true;
		} catch (Exception $e) {
			return false;
		}
	}
			
		
	
}
