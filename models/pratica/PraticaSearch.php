<?php

namespace app\models\pratica;

use app\models\pratica\Pratica;
use app\models\pratica\Cliente;
use yii\data\ActiveDataProvider;
use yii\helpers\VarDumper;


class PraticaSearch extends Pratica {

	public $id_pratica;
	public $codice_fiscale;
	
	public function rules() {
        return [
            [['codice_fiscale', 'id_pratica'], 'safe'],
        ];
    }
    
	public function search($params = []) {
		
		/**** DEFAULT ****/
		$query = Pratica::find()->joinWith('cliente');
		
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination'=> [ 'defaultPageSize' 	=> 5 ]
		]);
		
		/**** SORT ****/
		$dataProvider->sort->attributes['id'] = [
			'asc' => [ 'pratica.id' => SORT_ASC ],
			'desc' => [ 'pratica.id' => SORT_DESC ]
		];
		$dataProvider->sort->attributes['id_pratica'] = [
			'asc' => [ 'pratica.id_pratica' => SORT_ASC ],
			'desc' => [ 'pratica.id_pratica' => SORT_DESC ]
		];
		$dataProvider->sort->attributes['stato_pratica'] = [
			'asc' => [ 'pratica.stato_pratica' => SORT_ASC ],
			'desc' => [ 'pratica.stato_pratica' => SORT_DESC ]
		];
		
		
		/**** SEARCH ****/
		if ($this->load($params) && $this->validate()) {
			if ($this->id_pratica) {
				$query->andFilterWhere([ 'like', 'pratica.id_pratica', $this->id_pratica ]);
			}
			if ($this->codice_fiscale) {					
				$query->andFilterWhere([ 'like', 'cliente.codice_fiscale', $this->codice_fiscale ]);
			}
		}
		
		return $dataProvider;
	}
	
}
