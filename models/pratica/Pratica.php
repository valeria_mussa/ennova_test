<?php

namespace app\models\pratica;

use yii\db\ActiveRecord;

class Pratica extends ActiveRecord {
	
	public function getCliente() {
		return $this->hasOne(Cliente::className(), [ 'id' => 'id_cliente' ]);
	}
}
