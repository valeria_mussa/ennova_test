<?php

namespace app\controllers;

use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;
use app\models\ImportForm;

class ImportController extends Controller {

    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'import'],
                        'allow' => true,
                        'roles' => ['import'],
                    ]
                ],
            ]
        ];
    }
    
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
			return $this->redirect(['site/login']);
		}
		
		$model = new ImportForm();
		
		return $this->render('index', [ 'model' => $model]);
    }
    
    public function actionImport() {
		/*** JSON METHOD ***/
		Yii::$app->response->format = Response::FORMAT_JSON;
		try {
			//se non siamo dentro ad una post, torno all'index
			if (!Yii::$app->request->isPost) throw new Exception ( Yii::t('import','error_invalid_request'));
			
			//creo modulo e carico i dati del form nel modulo
			$model = new ImportForm();
					
			if (!$model->load(Yii::$app->request->post())) throw new Exception( Yii::t('import','error_cannot_load_model') );				
			$model->file = UploadedFile::getInstance($model, 'file');		
			
			//faccio l'upload del file per analizzarlo
			if (!$model->upload()) throw new Exception( Yii::t('import','error_cannot_upload_file') );
			
			//tronco prima la tabella (se richiesto e fattibile)
			$bool = $model->truncate();
			
			//verifico la tipologia di dato e lo carico in mysql
			if (!$model->insert($sql)) throw new Exception( Yii::t('import','error_cannot_import') );
			
			//cancello il file
			$model->removeFile();
			
			return [
				'success' => true,
				'message' => Yii::t('import','success_message') ,
				'query' => $sql
			];
        }
        catch (\Throwable $error) {
			
			//cancello il file
			$model->removeFile();
			
			 return [
				'success' => false, 
				'message' => Yii::t('import','error_cannot_import').': '.$error->getMessage()
			];
		}
	}

}
