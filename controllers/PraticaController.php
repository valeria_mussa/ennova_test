<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\bootstrap4\Html;
use app\models\pratica\Pratica;
use app\models\pratica\PraticaSearch;

class PraticaController extends Controller {
	
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view'],
                        'allow' => true,
                        'roles' => ['showList'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'view' => ['get'],
                ],
            ],
        ];
    }
    
    //TABELLA
	public function actionIndex() {
		
		if (Yii::$app->user->isGuest) {
			return $this->redirect(['site/login']);
		}
		
		try {
			$praticaSearch = new PraticaSearch();
			$dataProvider = $praticaSearch->search(Yii::$app->request->queryParams);
			$dataProviderAll = $praticaSearch->search();
		} catch (\Throwable $e) {
			throw new \yii\web\ServerErrorHttpException($e->getMessage());
		}
		$columns = $this->getGridColumns();
		
		return $this->render('index',[
			'dataProvider' => $dataProvider, 
			'dataProviderAll' => $dataProviderAll, 
			'praticaSearch' => $praticaSearch,
			'columns' => $columns
		]);
	}
	
	//DETTAGLI
	public function actionView() {
		
		if (Yii::$app->user->isGuest) {
			return $this->redirect(['site/login']);
		}
		
		$id = $this->request->get("id");
		
		try {
			$pratica = Pratica::find()->with('cliente')->where(['id' => $id])->limit(1)->asArray()->one();
		} catch (\Throwable $e) {
			throw new \yii\web\ServerErrorHttpException($e->getMessage());
		}
		
		if (!$pratica) throw new \yii\web\NotFoundHttpException(Yii::t('pratica','error_not_found'));
		
		return $this->render('view', [ 'pratica' => $pratica ]);
	}
	
	//la funzione restituisce le colonne necessarie per la gridView di visualizzazione dei dati e per la loro exportazione
	private function getGridColumns() {
		return [
			[
				'attribute' => 'id',
				'label' => Yii::t('pratica','id'),
				'format' => function($data) {
					return '#'.$data;
				}
			],
			[
				'attribute' => 'id_pratica',
				'label' => Yii::t('pratica','id_pratica'),
			],
			[
				'attribute' => 'data_creazione',
				'label' => Yii::t('pratica','data_creazione'),
				'format' => 'datetime',
				'hidden' => true,
				'hiddenFromExport' => false
			],
			[
				'attribute' => 'stato_pratica',
				'label' => Yii::t('pratica','stato_pratica'),
				'format' => function($data) {
					return Yii::t('pratica',$data);
				}
			],
			[
				'label' => Yii::t('pratica','cliente'),
				'value' => function($model) {
					return ($model['cliente']) ? $model['cliente']['nome'].' '.$model['cliente']['cognome'] : null;
				},
				'hidden' => true,
				'hiddenFromExport' => false
			],
			[
				'label' => Yii::t('pratica','codice_fiscale'),
				'value' =>  function($model) {
					return ($model['cliente']) ? $model['cliente']['codice_fiscale']: null;
				},
				'hidden' => true,
				'hiddenFromExport' => false
			],
			[ 
				'class' => 'kartik\grid\ActionColumn',
				'header' => '',
				'width' => 80,
				'contentOptions' => [ 'class' => 'text-center' ],
				'hiddenFromExport' => true,
				'template' => '{view}',
				'buttons' => [
					'view' => function($url, $model) {
						return Html::a(
							'<i class="fa-solid fa-magnifying-glass-plus"></i>','#',[
								'class' => 'show-details',
								'data-toggle' => 'modal',
								'data-target' => '#details',
								'data-url' => $url
							]
						);
					}
				]
			]
		];
	}
}
