<?php

return [
    'showList' => [
        'type' => 2,
        'description' => 'Show list page and export data',
    ],
    'import' => [
        'type' => 2,
        'description' => 'Import database data',
    ],
    'generic_user' => [
        'type' => 1,
        'children' => [
            'showList',
        ],
    ],
    'admin' => [
        'type' => 1,
        'children' => [
            'import',
            'generic_user',
        ],
    ],
];
