# TEST TECNICO - Mussa Valeria

<p>&nbsp;</p>


### 1 - DOWNLOAD SORGENTE
```
    git clone https://valeria_mussa@bitbucket.org/valeria_mussa/ennova_test.git
```

<p>&nbsp;</p>
### 2 - DOWNLOAD DIPENDENZE DA COMPOSER
```
    composer update
```
<p>&nbsp;</p>

### 3 - IMPORT AUTOMATICO

Collegandosi al portale web è previsto un mini pannello per la costruzione del db, 
che si occupa anche di verificare la presenza di tutti i privilegi di accesso necessari.

**_In caso di problemi, occorre invece eseguire manualmente gli step 4 e 5._**

<p>&nbsp;</p>

### 4 - AGGIUNTA PRIVILEGI

```
    ./config 	    +777	(per creazione automatica db)	
    ./runtime 	    +777 	(per esportazione)
    ./web/assets    +777    (per funzionamento yii)
```
<p>&nbsp;</p>

### 5 - CREAZIONE DATABASE

L'applicazione si aspetta un db chiamato `mv_ennova`, da configurare a piacere in `/config/db.php`

<p>&nbsp;</p>

### 6 - MIGRAZIONE TABELLE

La procedura non viene eseguita in automatico. Occorre dunque eseguire il comando

    ./yii migrate    
per la creazione delle tabelle e per l'aggiunta dei privilegi di accesso.

<p>&nbsp;</p>

### 7 - IMPORT DATA

I campioni di db da importare si trovano nella cartella `./db_import`

<p>&nbsp;</p>

# TECNOLOGIE USATE

LAMPP con:

- PHP 8.1.2
- MariaDB 10.4.22